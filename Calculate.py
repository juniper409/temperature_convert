#Program: Temperature Convert
#Created by: Juniper409 - mrjuniper409@gmail.com - juniper409 on Gitlab
#Language: Python 3.6
#Purpose: Converts fahrenheit temperature to the celsius temperature
#--------------------------------------------------------------------------------


#Print user Options
print("Please select the following options:")
print("1. Convert fahrenheit to celsus.")
print("2. Convert celsius to fahrenheit.")

while(True):
    #Asks for user Selection
    option_select = float(input('selection: '))
    num = int(input('Temperature to convert: '))

    #Calculations used based on user's option selection
    f_calc = (num - 32) * .55555556
    c_calc = (num * .55555556)/32

    #Determines which calculation to use based on the user's option selection.
    if option_select == int('1'):
        print(round(f_calc))

    elif option_select == int('2'):
        print(round(c_calc))

    else:
        print('Invalid Input. Please try again.')

else:
    print("Something went wrong. Please try again.")



#        num = int(input('Please enter the temperature in fahrenheit:'))
#        calculation = (num - 32) * .55555556
#        round_calculation = round(calculation)
#        print(f, 'degrees fahrenheit is', round_calculation,'degrees celsius.')
